package model.data_structures;

public class Node<E> extends NodoListaSencilla<E>
{

	/**
	 * Constante de serializaci�n
	 */
	private static final long serialVersionUID = 14546221L;
	
	/**
	 * Nodo anterior.
	 */
	//TODO Declarar el nodo anterior.
	private Node<E> anterior;
	
	/**
	 * M�todo constructor del nodo doblemente encadenado
	 * @param elemento elemento que se almacenar� en el nodo.
	 */
	public Node(E elemento) 
	{
		super(elemento);
		anterior = null;
	}
	
	/**
	 * M�todo que retorna el nodo anterior.
	 * @return Nodo anterior.
	 */
	public Node<E> darAnterior()
	{
		//TODO Completar el m�todo de acuerdo a la documentaci�n.
		return anterior;
	}
	
	/**
	 * M�todo que cambia el nodo anterior por el que llega como par�metro.
	 * @param anterior Nuevo nodo anterior.
	 */
	public void cambiarAnterior(Node<E> pAnterior)
	{
		//TODO Completar el m�todo de acuerdo a la documentaci�n.
		anterior = pAnterior;
	}
}
