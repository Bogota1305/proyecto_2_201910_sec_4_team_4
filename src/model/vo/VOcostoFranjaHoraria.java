package model.vo;

public class VOcostoFranjaHoraria implements Comparable<VOcostoFranjaHoraria>  {
	
	private String fecha;

	private int hora;

	private int cantidad;

	private int sinInfraccion;
	
	private int conInfraccion;
	
	
	private int valorAcumulado;

	public VOcostoFranjaHoraria(String f, int h, int c, int sin, int con, int v )
	{
		fecha=f;
		hora = h;
		cantidad = c;
		sinInfraccion = sin;
		conInfraccion = con;
		valorAcumulado=v;
	}
	
	public String darFecha() {
		return fecha;
	}

	public int darHora() {
		return hora;
	}

	public int darCantidad(){
		return cantidad;
	}
	
	public int darSin(){
		return sinInfraccion;
	}
	
	public int darCon(){
		return conInfraccion;
	}
	
	public int darValorAcumulado()
	{
		return valorAcumulado;
	}

	@Override
	public int compareTo(VOcostoFranjaHoraria o) {
		// TODO Auto-generated method stub
		
		if(this.valorAcumulado<o.valorAcumulado)
		{
			return 1;
		}
		else if(this.valorAcumulado>o.valorAcumulado)
		{
			return -1;
		}
		
		return 1;
	}

}