package model.vo;

public class VOOrdenarxycoord implements Comparable<VOOrdenarxycoord>{

	private int penalty1;
	private double xCoord;
	private double yCoord;
	private String accidentIndicator;
	private String location;
	private int addressId;
	private String streetsegid;

	public VOOrdenarxycoord(int p, double x, double y, String a, String l, int adId, String stId)
	{
		penalty1  = p;
		xCoord = x;
		yCoord = y;
		accidentIndicator = a;
		location = l;
		addressId = adId;
		streetsegid = stId;
	}

	public int darpenalty()
	{
		return penalty1;
	}

	public String accidentIndicator()
	{
		return accidentIndicator;
	}

	public double darX()
	{
		return xCoord;
	}

	public double darY()
	{
		return yCoord;
	}

	public String darLocation() 
	{
		return location;
	}
	
	public String darStreetSegId() 
	{
		return streetsegid;
	}
	
	public int darAddressId() 
	{
		return addressId;
	}


	@Override
	public int compareTo(VOOrdenarxycoord o) {
		// TODO Auto-generated method stub

		if(this.xCoord>o.xCoord)
		{
			return 1;
		}
		else if(this.xCoord<o.xCoord)
		{
			return -1;
		}
		else
		{
			if(this.yCoord>o.yCoord)
			{
				return 1;
			}
			else if(this.yCoord<o.yCoord)
			{
				return -1;
			}
		}

		return 1;

	}

}
