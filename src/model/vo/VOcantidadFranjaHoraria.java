package model.vo;

public class VOcantidadFranjaHoraria implements Comparable<VOcantidadFranjaHoraria>  {

	private int hora;

	private int cantidad;

	private int sinInfraccion;
	
	private int conInfraccion;
	
	private int pagadoInfraccion;

	public VOcantidadFranjaHoraria(int h, int c, int sin, int con, int paga )
	{
		hora = h;
		cantidad = c;
		sinInfraccion = sin;
		conInfraccion = con;
		pagadoInfraccion = paga;
	}

	public int darHora() {
		return hora;
	}

	public int darCantidad(){
		return cantidad;
	}
	
	public int darSin(){
		return sinInfraccion;
	}
	
	public int darCon(){
		return conInfraccion;
	}
	
	public int darPorPagar(){
		return pagadoInfraccion;
	}

	@Override
	public int compareTo(VOcantidadFranjaHoraria o) {
		// TODO Auto-generated method stub
		
		if(this.cantidad<o.cantidad)
		{
			return 1;
		}
		else if(this.cantidad>o.cantidad)
		{
			return -1;
		}
		
		return 1;
	}

}
