package model.vo;

public class VOCantidadXYCoord implements Comparable<VOCantidadXYCoord> {
	
	private double xCoord;
	
	private double yCoord;
	
	private int cantidad;

	private int sinInfraccion;

	private int conInfraccion;

	private int pagadoInfraccion;
	
	private String location;
	
	private int addressId;
	
	private String streetsegid;
	
    private double conPorcentaje;
	
	private double sinPorcentaje;

	public VOCantidadXYCoord(double x, double y, int c, int sin, int con, int paga, String l, int adId, String stId, double conPor, double sinPor )
	{
		xCoord = x;
		yCoord = y;
		cantidad = c;
		sinInfraccion = sin;
		conInfraccion = con;
		pagadoInfraccion = paga;
		location = l;
		addressId = adId;
		streetsegid = stId;
		conPorcentaje = conPor;
		sinPorcentaje = sinPor;
	}


	public int darCantidad(){
		return cantidad;
	}

	public int darSin(){
		return sinInfraccion;
	}

	public int darCon(){
		return conInfraccion;
	}

	public int darPorPagar(){
		return pagadoInfraccion;
	}
	
	public double darX()
	{
		return xCoord;
	}

	public double darY()
	{
		return yCoord;
	}

	public String darLocation() 
	{
		return location;
	}
	
	public String darStreetSegId() 
	{
		return streetsegid;
	}
	
	public int darAddressId() 
	{
		return addressId;
	}

	public double darConPor()
	{
		return conPorcentaje;
	}
	
	public double darSinPor()
	{
		return sinPorcentaje;
	}

	@Override
	public int compareTo(VOCantidadXYCoord o) {
		// TODO Auto-generated method stub
		if(this.cantidad<o.darCantidad())
		{
			return 1;
		}
		else if(this.cantidad>o.darCantidad())
		{
			return -1;
		}
		return 1;
	}

}
