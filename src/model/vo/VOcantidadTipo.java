package model.vo;

public class VOcantidadTipo implements Comparable<VOcantidadTipo> {

	private String tipo;

	private int cantidad;

	private int sinInfraccion;
	
	private int conInfraccion;
	
	private int pagadoInfraccion;

	public VOcantidadTipo(String t, int c, int sin, int con, int paga )
	{
		tipo = t;
		cantidad = c;
		sinInfraccion = sin;
		conInfraccion = con;
		pagadoInfraccion = paga;
	}

	public String darTipo() {
		return tipo;
	}

	public int darCantidad(){
		return cantidad;
	}
	
	public int darSin(){
		return sinInfraccion;
	}
	
	public int darCon(){
		return conInfraccion;
	}
	
	public int darPorPagar(){
		return pagadoInfraccion;
	}

	public int compareTo(VOcantidadTipo o) {
		// TODO Auto-generated method stub
		
		if(this.darCantidad() <= o.darCantidad()){
			
			return 1;
		}
		
		else if(this.darCantidad()> o.darCantidad()){
				
			return -1;
		}
		
		return 1;
	}

}
