package model.vo;

public class VOordenarHora implements Comparable<VOordenarHora> {

	private int penalty1;
	private int hora;
	private String accidentIndicator;
	
	public VOordenarHora(int p, int t, String a)
	{
		penalty1  = p;
		hora = t;
		accidentIndicator = a;
	}
	
	public int darpenalty()
	{
		return penalty1;
	}
	
	public int darHora()
	{
		return hora;
	}
	
	public String accidentIndicator()
	{
		return accidentIndicator;
	}

	@Override
	public int compareTo(VOordenarHora o) {
		// TODO Auto-generated method stub
		if(this.hora>o.hora)
		{
			return 1;
		}
		else if(this.hora<o.hora)
		{
			return -1;
		}
		
		return 1;
	}
	
}
