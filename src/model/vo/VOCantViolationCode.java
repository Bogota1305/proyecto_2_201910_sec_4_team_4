package model.vo;

public class VOCantViolationCode implements Comparable<VOCantViolationCode>{

private String ViolationCode;
	
	private double cantidad;

	public VOCantViolationCode(String vc, double fa )
	{
		ViolationCode = vc;
		
		cantidad = fa;
	}
	
	public String getViolationCode() {
		return ViolationCode;
	}
	
	public double getCantidad() {
		return cantidad;
	}

	@Override
	public int compareTo(VOCantViolationCode o) {
		// TODO Auto-generated method stub
		if(this.cantidad>o.getCantidad())
		{
			return -1;
		}
		else if(this.cantidad<o.getCantidad())
		{
			return 1;
		}
		return 1;
	}

	
}
