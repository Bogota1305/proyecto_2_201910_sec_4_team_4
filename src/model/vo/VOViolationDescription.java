package model.vo;

public class VOViolationDescription implements Comparable<VOViolationDescription> {
	
	private String ViolationDescription;
	
	private int objectId;
	
	private String ticketIssueDate;

	public VOViolationDescription(String vd, int oi, String tid)
	{
		ViolationDescription = vd;
		
		objectId = oi;
		
		ticketIssueDate = tid;
		
	}
	
	public String getViolationDescription() {
		return ViolationDescription;
	}
	
	public int objectId() {
		return objectId;
	}
	
	public String getTicketIssueDate() {
		
		return ticketIssueDate;
	}
	


	@Override
	public int compareTo(VOViolationDescription o) {
		// TODO Auto-generated method stub
		
		if(this.getViolationDescription().compareTo(o.getViolationDescription())>0){
			
			return 1;
		}
		
		else if(this.getViolationDescription().compareTo(o.getViolationDescription())<0){
				
			return -1;
		}
		
		return 1;
	}
}
