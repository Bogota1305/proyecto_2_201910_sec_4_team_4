package model.vo;

public class VOViolationCode implements Comparable<VOViolationCode>{
	
	private String ViolationCode;
	
	private double FineAmt;

	public VOViolationCode(String vc, double fa )
	{
		ViolationCode = vc;
		
		FineAmt = fa;
	}
	
	public String getViolationCode() {
		return ViolationCode;
	}
	
	public double getAvgFineAmt() {
		return FineAmt;
	}

	@Override
	public int compareTo(VOViolationCode o) {
		// TODO Auto-generated method stub

		if(this.getViolationCode().compareTo(o.getViolationCode())>0){
			
			return 1;
		}
		
		else if(this.getViolationCode().compareTo(o.getViolationCode())<0){
				
			return -1;
		}
		
		return 1;
	}
}