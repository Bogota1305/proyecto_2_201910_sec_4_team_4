package model.vo;

public class VOcantidadFecha implements Comparable<VOcantidadFecha> {

	private String fecha;

	private int cantidad;

	private int sinInfraccion;
	
	private int conInfraccion;
	
	private int pagadoInfraccion;

	public VOcantidadFecha(String f, int c, int sin, int con, int paga )
	{
		fecha = f;
		cantidad = c;
		sinInfraccion = sin;
		conInfraccion = con;
		pagadoInfraccion = paga;
	}

	public String darFecha() {
		return fecha;
	}

	public int darCantidad(){
		return cantidad;
	}
	
	public int darSin(){
		return sinInfraccion;
	}
	
	public int darCon(){
		return conInfraccion;
	}
	
	public int darPorPagar(){
		return pagadoInfraccion;
	}

	@Override
	public int compareTo(VOcantidadFecha o) {
		// TODO Auto-generated method stub
		
		if(this.darFecha().compareTo(o.darFecha())>=0){
			
			return 1;
		}
		
		else if(this.darFecha().compareTo(o.darFecha())<0){
				
			return -1;
		}
		
		return 1;
	}

}
