package model.vo;

import java.util.Date;

/**
 * Representation of a MovingViolation object
 */
public class VOMovingViolations implements Comparable<VOMovingViolations> {

	private int objectId;
	private String location;
	private double totalPaid;
	private int fineAmt;
	private int penalty1;
	private String ticketIssueDate;
	private String accidentIndicator;
	private String violationDescription;
	private String violatioCode;
	private int addressId;
	private String streetsegid;
	private double xCoord;
	private double yCoord;

	public VOMovingViolations(int pOjectId, String pLocation, double pTotalPaid, String pTicketIssueDate, String pAccidentIndicator, String pViolationDescription,String pViolatioCode, int pFineAmt, int pPenalty1, int pAddressId, String pStreetsegid, double x, double y)
	{
		objectId = pOjectId;
		location = pLocation;
		totalPaid = pTotalPaid;
		ticketIssueDate = pTicketIssueDate;
		accidentIndicator = pAccidentIndicator;
		violationDescription = pViolationDescription;
		violatioCode = pViolatioCode;
		fineAmt = pFineAmt;
		penalty1 = pPenalty1;
		addressId = pAddressId;
		streetsegid = pStreetsegid;
		xCoord = x;
		yCoord = y;
		
	}

	/**
	 * @return id - Identificador �nico de la infracci�n
	 */
	public int objectId() {
		// TODO Auto-generated method stub
		return objectId;
	}	


	/**
	 * @return location - Direcci�n en formato de texto.
	 */
	public String getLocation() {
		// TODO Auto-generated method stub
		return location;
	}

	/**
	 * @return date - Fecha cuando se puso la infracci�n .
	 */
	public String getTicketIssueDate() {
		// TODO Auto-generated method stub
		return ticketIssueDate;
	}

	/**
	 * @return totalPaid - Cuanto dinero efectivamente pag� el que recibi� la infracci�n en USD.
	 */
	public double getTotalPaid() {
		// TODO Auto-generated method stub
		return totalPaid;
	}

	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		// TODO Auto-generated method stub
		return accidentIndicator;
	}

	/**
	 * @return description - Descripci�n textual de la infracci�n.
	 */
	public String  getViolationDescription() {
		// TODO Auto-generated method stub
		return violationDescription;
	}

	@Override
	public int compareTo(VOMovingViolations o) {
		// TODO implementar la comparacion "natural" de la clase

		if(this.getTicketIssueDate().compareTo(o.getTicketIssueDate())>=0){
				
			return 1;
		}
		
		else if(this.getTicketIssueDate().compareTo(o.getTicketIssueDate())<0){
				
			return -1;
		}
		
		else{
			
			if(this.objectId> o.objectId){
				
				return 1;
			}
			
			return -1;
		
		}
	}
	
	public String toString()
	{
		// TODO Convertir objeto en String (representacion que se muestra en la consola)
		return "-";
	}


	public String getViolationCode() {

		return violatioCode;
	}
	public String getStreetSegId() {
		return streetsegid;
	}
	
	public int getAddressId() {
		return addressId;
	}
	
	public int getFineAmt()
	{
		return fineAmt;
	}
	
	public int getPenalty1()
	{
		return penalty1;
	}
	
	public double getX()
	{
		return xCoord;
	}
	
	public double getY()
	{
		return yCoord;
	}
}
