package model.logic;

import java.io.FileNotFoundException;

import java.io.FileReader;
import java.io.IOException;

import java.text.ParseException;
import com.opencsv.CSVReader;

import model.data_structures.Cola;
import model.data_structures.LinearProbingHashST;
import model.data_structures.PQueue;
import model.vo.VOMovingViolations;

public class MovingViolationsManager {

	private Cola<VOMovingViolations> pCola = new Cola<>();
	private LinearProbingHashST<Integer,PQueue<VOMovingViolations>> LinearHash = new LinearProbingHashST<Integer, PQueue<VOMovingViolations>>();
	
	private int cont;
	Comparable<VOMovingViolations> [ ] muestra;


	public int loadMovingViolations(String movingViolationsFile){
		// TODO Auto-generated method stub
		
		cont = 0;

		try {
			@SuppressWarnings("resource")
			CSVReader Csvr = new CSVReader(new FileReader(movingViolationsFile));
			String[] lector = new String[17];
			
			lector = Csvr.readNext();
			
			while((lector = Csvr.readNext()) != null)
			{
				

				if(lector != null)
				{
					if(movingViolationsFile.equals("./data/Moving_Violations_Issued_in_December_2018.csv") || movingViolationsFile.equals("./data/Moving_Violations_Issued_in_November_2018.csv")  || movingViolationsFile.equals("./data/Moving_Violations_Issued_in_October_2018.csv"))
					{
						VOMovingViolations infraccion = new VOMovingViolations(Integer.parseInt(lector[0]), lector[2], Integer.parseInt(lector[9]),  lector[14] , lector[12], lector[16], lector[15], Integer.parseInt(lector[8]), Integer.parseInt(lector[10]), lector[3].equals("")?0:Integer.parseInt(lector[3]), lector[4], Double.parseDouble(lector[5]), Double.parseDouble(lector[6]));
						pCola.enqueue(infraccion);		
					}
					else
					{
						VOMovingViolations infraccion = new VOMovingViolations(Integer.parseInt(lector[0]), lector[2], Double.parseDouble(lector[9]),  lector[13] , lector[12], lector[15], lector[14],  Integer.parseInt(lector[8]), Integer.parseInt(lector[10]), lector[3].equals("")?0:Integer.parseInt(lector[3]), lector[4], Double.parseDouble(lector[5]), Double.parseDouble(lector[6]));
						pCola.enqueue(infraccion);
					}
							
					cont++;

				}
			}
		

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return cont;
	}

	public Cola<VOMovingViolations> darCola()
	{
		return this.pCola;
	}
	public LinearProbingHashST<Integer, PQueue<VOMovingViolations>> darLinear()
	{
		return LinearHash;
	}

	public int darNumeroInfracciones()
	{
		return cont;
	}
	
	public void vaciar()
	{
		pCola = new Cola<>();
		LinearHash = new LinearProbingHashST<Integer, PQueue<VOMovingViolations>>();
	}

}
