package controller;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.Scanner;
import org.javatuples.*;

import model.data_structures.BST;
import model.data_structures.Cola;
import model.data_structures.LinearProbingHashST;
import model.data_structures.PQueue;
import model.data_structures.RedBlackBST;
import model.logic.MovingViolationsManager;
import model.util.Sort;
import model.util.StdRandom;
import model.vo.VOCantViolationCode;
import model.vo.VOCantidadXYCoord;
import model.vo.VOMovingViolations;
import model.vo.VOOrdenarxycoord;
import model.vo.VOcantidadFecha;
import model.vo.VOcantidadFranjaHoraria;
import model.vo.VOcantidadTipo;
import model.vo.VOcostoFranjaHoraria;
import model.vo.VOordenarHora;
import view.MovingViolationsManagerView;


public class Controller {

	private MovingViolationsManagerView view;

	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */
	private Cola<VOMovingViolations> movingViolationsPQueue;


	private MovingViolationsManager manager;

	private VOMovingViolations[] muestra;


	public Controller() {
		view = new MovingViolationsManagerView();

		manager=new MovingViolationsManager() ;

		//TODO, inicializar la pila y la cola
		movingViolationsPQueue = new Cola<VOMovingViolations>();
		muestra=null;
	}

	public void run() {
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		Controller controller = new Controller();

		while(!fin)
		{
			view.printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 0:
				view.printMessage("Elija el semestre a cargar (1 ó 2)");
				int sem = Integer.parseInt(sc.next());
				view.printMessage("Se cargaran los datos");
				long startTimeA = System.currentTimeMillis();
				controller.loadMovingViolations(sem);
				long endTimeA = System.currentTimeMillis();
				long durationA = endTimeA - startTimeA;
				System.out.println(durationA);
				break;

			case 1:	

				view.printMessage("Ingresar valor de N: ");
				int cant = Integer.parseInt(sc.next());
				controller.darRankingInfraccionesPorFranjaHoraria(cant);
				break;

			case 2:	

				view.printMessage("Ingrese la coordenada en X de la localizacion geografica (Ej. 1234,56): ");
				double xcoord = sc.nextDouble();
				view.printMessage("Ingrese la coordenada en Y de la localizacion geografica (Ej. 5678,23): ");
				double ycoord = sc.nextDouble();
				controller.ordenanimentoLocalizacion(xcoord, ycoord);
				break;

			case 3:	
				view.printMessage("Ingrese la fecha inicial del rango. Formato a�o-mes-dia (ej. 2008-06-21)");
				String fechaInicialStr = sc.next();
				LocalDate fechaInicial = controller.convertirFecha( fechaInicialStr );

				view.printMessage("Ingrese la fecha final del rango. Formato a�o-mes-dia (ej. 2008-06-30)");
				String fechaFinalStr = sc.next();
				LocalDate fechaFinal = controller.convertirFecha( fechaFinalStr );

				controller.consultarInfraccionesPorRangoFechas(fechaInicial, fechaFinal);

				break;

			case 4:	

				view.printMessage("1A. Consultar los N tipos de infracción(ViolationCode) con mas infracciones que desea ver. Ingresar valor de N: ");
				int cantB = Integer.parseInt(sc.next());
				controller.darRankingInfraccionesPorTipo(cantB);
				break;

			case 5:

				view.printMessage("Ingrese la coordenada en X de la localizacion geografica (Ej. 1234,56): ");
				double xcoord1 = sc.nextDouble();
				view.printMessage("Ingrese la coordenada en Y de la localizacion geografica (Ej. 5678,23): ");
				double ycoord1 = sc.nextDouble();
				controller.ordenanimentoLocalizacionEnArbolBalanceado(xcoord1, ycoord1);
				break;
				
			case 6:
				
				view.printMessage("Ingrese el valor minimo: ");
				int minimo = sc.nextInt();
				view.printMessage("Ingrese el valor maximo: ");
				int maximo = sc.nextInt();
				controller.consultarFranjasEnRango(minimo, maximo);
				
				break;
				

			case 7: 

				view.printMessage("Ingrese el AddressID: ");
				int addId = sc.nextInt();
				int startTimeB = (int) System.currentTimeMillis();
				controller.darInformacionLocalizacion(addId);
				int endTimeB = (int) System.currentTimeMillis();
				int durationB = endTimeB - startTimeB;
				System.out.println("Tiempo de ejecucion: " + durationB);

				break;

			case 8: 

				view.printMessage("Ingrese la hora inicial (Ej. 00:00:00)");
				String HoraInicial = sc.next();
				view.printMessage("Ingrese la hora final (Ej. 23:59:00)");
				String HoraFinal = sc.next();
				int startTimeC = (int) System.currentTimeMillis();
				controller.infraccionesRangoHora(HoraInicial, HoraFinal);
				int endTimeC = (int) System.currentTimeMillis();
				int durationC = endTimeC - startTimeC;
				System.out.println("Tiempo de ejecucion: " + durationC);
				break;

			case 9: 

				view.printMessage("Ingresar valor de N: (max 4967) ");
				int cant1 = Integer.parseInt(sc.next());
				int startTimeD = (int) System.currentTimeMillis();
				controller.rankingCoordenadas(cant1);
				int endTimeD = (int) System.currentTimeMillis();
				int durationD = endTimeD - startTimeD;
				System.out.println("Tiempo de ejecucion: " + durationD);
				break;
				
			case 10:
				
				int startTimeE = (int) System.currentTimeMillis();
				controller.darGraficaPorCodigo();
				int endTimeE = (int) System.currentTimeMillis();
				int durationE = endTimeE - startTimeE;
				System.out.println("Tiempo de ejecucion: " + durationE);
				break;

			case 11:	
				fin=true;
				sc.close();
				break;
			}
		}

	}


	public VOMovingViolations [ ] generarMuestra( int n )
	{
		muestra = new VOMovingViolations[ n ];
		movingViolationsPQueue = manager.darCola();


		int i = 0;
		while(i<n && !movingViolationsPQueue.isEmpty())
		{
			muestra[i] = movingViolationsPQueue.dequeue();
			System.out.println(muestra[i].toString());

			i++;
		}


		StdRandom.shuffle(muestra);

		// TODO Llenar la muestra aleatoria con los datos guardados en la estructura de datos

		return muestra;

	}


	public void loadMovingViolations(int semestre) {
		// TODO

		int enerojulio = 0;
		int febreroAgosto = 0;
		int marzoSeptiempre = 0;
		int abrilOctubre = 0;
		int mayoNoviembre = 0;
		int junioDiciembre = 0;

		if(semestre==1)
		{
			if(manager.darCola()!=null)
			{
				manager.vaciar();
			}
			enerojulio = manager.loadMovingViolations("./data/Moving_Violations_Issued_in_January_2018_ordered.csv");
			febreroAgosto = manager.loadMovingViolations("./data/Moving_Violations_Issued_in_February_2018_ordered.csv");
			marzoSeptiempre = manager.loadMovingViolations("./data/Moving_Violations_Issued_in_March_2018.csv");
			abrilOctubre = manager.loadMovingViolations("./data/Moving_Violations_Issued_in_April_2018.csv");
			mayoNoviembre = manager.loadMovingViolations("./data/Moving_Violations_Issued_in_May_2018.csv");
			junioDiciembre = manager.loadMovingViolations("./data/Moving_Violations_Issued_in_June_2018.csv");
			int cantidadA;
			cantidadA = enerojulio+febreroAgosto+marzoSeptiempre+abrilOctubre+mayoNoviembre+junioDiciembre;
			movingViolationsPQueue = manager.darCola();

			view.printMessage("");
			view.printMessage("");
			view.printMessage("Infracciones enero: " + enerojulio);
			view.printMessage("Infracciones febrero: " + febreroAgosto);
			view.printMessage("Infracciones marzo: " + marzoSeptiempre);
			view.printMessage("Infracciones abril: " + abrilOctubre);
			view.printMessage("Infracciones mayo: " + mayoNoviembre);
			view.printMessage("Infracciones junio: " + junioDiciembre);
			view.printMessage("");
			int[] retornar = new int[4];
			int xMax = 0;
			int yMax = 0;
			int xMin = 99999999;
			int yMin = 99999999;

			for(VOMovingViolations v : manager.darCola())
			{
				if(v.getX()<xMin)
				{
					xMin = (int) v.getX();
				}
				else if(v.getX()>xMax)
				{
					xMax = (int) v.getX();
				}
				else if(v.getY()<yMin)
				{
					yMin = (int) v.getY();
				}
				else if(v.getY()>yMax)
				{
					yMax = (int) v.getY();
				}

			}

			retornar[0] = xMin;
			retornar[1] = yMin;
			retornar[2] = xMax;
			retornar[3] = yMax;

			view.printMessage("Las cordenadas son: Min(" + retornar[0] + "x, " + retornar[1] +"y)" + ", Max("+ retornar[2] + "x, " + retornar[3] +"y)");
			view.printMessage("------------------------------------------------------");
			view.printMessage("Se cargaron " + cantidadA + " datos de Enero a Junio");
			view.printMessage("------------------------------------------------------");
			view.printMessage("");
		}
		if(semestre == 2)
		{
			if(manager.darCola()!=null)
			{
				manager.vaciar();
			}
			enerojulio = manager.loadMovingViolations("./data/Moving_Violations_Issued_in_July_2018.csv");
			febreroAgosto = manager.loadMovingViolations("./data/Moving_Violations_Issued_in_August_2018.csv");
			marzoSeptiempre = manager.loadMovingViolations("./data/Moving_Violations_Issued_in_September_2018.csv");
			abrilOctubre = manager.loadMovingViolations("./data/Moving_Violations_Issued_in_October_2018.csv");
			mayoNoviembre = manager.loadMovingViolations("./data/Moving_Violations_Issued_in_November_2018.csv");
			junioDiciembre = manager.loadMovingViolations("./data/Moving_Violations_Issued_in_December_2018.csv");
			int cantidadA;
			cantidadA = enerojulio+febreroAgosto+marzoSeptiempre+abrilOctubre+mayoNoviembre+junioDiciembre;
			movingViolationsPQueue = manager.darCola();

			view.printMessage("");
			view.printMessage("");
			view.printMessage("Infracciones julio: " + enerojulio);
			view.printMessage("Infracciones agosto: " + febreroAgosto);
			view.printMessage("Infracciones septiembre: " + marzoSeptiempre);
			view.printMessage("Infracciones octubre: " + abrilOctubre);
			view.printMessage("Infracciones noviembre: " + mayoNoviembre);
			view.printMessage("Infracciones diciembre: " + junioDiciembre);
			view.printMessage("");
			int[] retornar = new int[4];
			int xMax = 0;
			int yMax = 0;
			int xMin = 99999999;
			int yMin = 99999999;

			for(VOMovingViolations v : manager.darCola())
			{
				if(v.getX()<xMin)
				{
					xMin = (int) v.getX();
				}
				else if(v.getX()>xMax)
				{
					xMax = (int) v.getX();
				}
				else if(v.getY()<yMin)
				{
					yMin = (int) v.getY();
				}
				else if(v.getY()>yMax)
				{
					yMax = (int) v.getY();
				}

			}

			retornar[0] = xMin;
			retornar[1] = yMin;
			retornar[2] = xMax;
			retornar[3] = yMax;

			view.printMessage("Las cordenadas son: Min(" + retornar[0] + "x, " + retornar[1] +"y)" + ", Max("+ retornar[2] + "x, " + retornar[3] +"y)");
			view.printMessage("---------------------------------------------------------");
			view.printMessage("Se cargaron " + cantidadA + " datos de Julio a Diciembre");
			view.printMessage("---------------------------------------------------------");
			view.printMessage("");
		}

	}


	//Parte A
	//1-A
	private PriorityQueue<VOcantidadFranjaHoraria>  darRankingInfraccionesPorFranjaHoraria (int nCantidad)
	{
		PriorityQueue<VOcantidadFranjaHoraria> retornar = new PriorityQueue<VOcantidadFranjaHoraria>();
		VOcantidadFranjaHoraria[] ordenar = new VOcantidadFranjaHoraria[24];
		VOordenarHora[] copia = new VOordenarHora[movingViolationsPQueue.size()];

		int i = 0;

		for(VOMovingViolations v : movingViolationsPQueue )
		{
			LocalDateTime horaA = convertirFecha_Hora_LDT(v.getTicketIssueDate());
			copia[i] = new VOordenarHora(v.getPenalty1(), horaA.getHour(), v.getAccidentIndicator());
			i++;
		}

		Sort.ordenarMergeSort(copia);

		int hora = 0;
		int cantidad = 0;
		int sinInfraccion = 0;
		int conInfraccion = 0;
		int pagadoInfraccion = 0;
		int k = 0;

		for(int j = 0; j<copia.length; j++)
		{
			int horaComp = copia[j].darHora();

			if(hora ==	horaComp && j+1!=copia.length)
			{
				cantidad++;

				if(copia[j].accidentIndicator().equals("Yes"))
				{
					conInfraccion++;
				}
				else
				{
					sinInfraccion++;
				}

				pagadoInfraccion += copia[j].darpenalty();
			}
			else
			{

				conInfraccion = (conInfraccion*100)/cantidad;
				sinInfraccion = (sinInfraccion*100)/cantidad;
				conInfraccion++;

				ordenar[k] = new VOcantidadFranjaHoraria(hora, cantidad, sinInfraccion, conInfraccion, pagadoInfraccion);
				k++;
				conInfraccion = 0;
				sinInfraccion = 0;
				cantidad = 0;
				hora++;
			}


		}

		k = 0;


		Sort.ordenarMergeSort(ordenar);



		while(k<nCantidad)
		{
			retornar.add(ordenar[k]);
			k++;

		}

		k = 0;

		for(VOcantidadFranjaHoraria v: retornar)
		{
			System.out.println("");
			System.out.println("Franja horaria: " + v.darHora()+":00:00-" + v.darHora()+":59:59");
			System.out.println("Cantidad infracciones: " + v.darCantidad());
			System.out.println("Porcentaje infracciones con accidente : " + v.darCon()+ "%");
			System.out.println("Porcentaje infracciones sin accidente : " + v.darSin()+ "%");
			System.out.println("Cantidad por pagar: " + "$" + v.darPorPagar());
			System.out.println("");
		}



		return retornar;
	}

	//2A

	public VOCantidadXYCoord ordenanimentoLocalizacion(double xCoord, double yCoord)
	{
		VOOrdenarxycoord[] copia = new VOOrdenarxycoord[movingViolationsPQueue.size()];
		VOCantidadXYCoord retornar = null;
		LinearProbingHashST<Double, VOCantidadXYCoord> tablaHash = new LinearProbingHashST<Double, VOCantidadXYCoord>();
		int i = 0;

		for(VOMovingViolations v : movingViolationsPQueue )
		{
			copia[i] = new VOOrdenarxycoord(v.getPenalty1(), v.getX(), v.getY(), v.getAccidentIndicator(), v.getLocation(), v.getAddressId(), v.getStreetSegId());
			i++;
		}

		Sort.ordenarMergeSort(copia);

		int cantidad = 0;
		int sinInfraccion = 0;
		int conInfraccion = 0;
		int pagadoInfraccion = 0;

		for(int j = 0; j<copia.length; j++)
		{
			double xCo = copia[j].darX();
			double yCo = copia[j].darY();
			String location = copia[j].darLocation();
			int adId = copia[j].darAddressId();
			String stId = copia[j].darStreetSegId();

			while(xCo == copia[j].darX() && yCo == copia[j].darY() && j<copia.length)
			{
				cantidad++;

				if(copia[j].accidentIndicator().equals("Yes"))
				{
					conInfraccion++;
				}
				else
				{
					sinInfraccion++;
				}

				pagadoInfraccion += copia[j].darpenalty();

				j++;
			}

			conInfraccion = (conInfraccion*100)/cantidad;
			sinInfraccion = (sinInfraccion*100)/cantidad;

			double llave = xCo + yCo + (xCo/yCo);

			tablaHash.put(llave, new VOCantidadXYCoord(xCo, yCo, cantidad, sinInfraccion, conInfraccion, pagadoInfraccion, location, adId, stId, 0, 0));

			conInfraccion = 0;
			sinInfraccion = 0;
			cantidad = 0;
		}
		System.out.println(tablaHash.size());

		double llaveretornar = xCoord + yCoord + (xCoord/yCoord);

		retornar = tablaHash.get(llaveretornar);

		System.out.println("");
		System.out.println("Cantidad infracciones: " + retornar.darCantidad());
		System.out.println("Porcentaje infracciones sin accidente : "  + retornar.darSin() + "%");
		System.out.println("Porcentaje infracciones con accidente : "  + retornar.darCon() + "%");
		System.out.println("Cantidad por pagar: " + "$" + retornar.darPorPagar());
		System.out.println("Location: " + retornar.darLocation());
		System.out.println("AddressId: " + retornar.darAddressId());
		System.out.println("StreetSegId: " + retornar.darStreetSegId());
		System.out.println("");

		return retornar;
	}


	//3A
	public void consultarInfraccionesPorRangoFechas(LocalDate fechaInicial, LocalDate fechaFinal)
	{
		VOMovingViolations[] copia = new VOMovingViolations[movingViolationsPQueue.size()];
		RedBlackBST<String, VOcantidadFecha> retornar = new RedBlackBST<String, VOcantidadFecha>();

		int i = 0;

		for(VOMovingViolations v : movingViolationsPQueue )
		{
			copia[i] = v;
			i++;
		}

		Sort.ordenarMergeSort(copia);

		for(int j = 0; j<copia.length ; j++)
		{
			String[] aux = copia[j].getTicketIssueDate().split("T");
			LocalDate fecha = convertirFecha(aux[0]);
			LocalDate fecha1 = convertirFecha(aux[0]);

			int cantidad = 0;
			int sinInfraccion = 0;
			int conInfraccion = 0;
			int pagadoInfraccion = 0;

			while(fecha.compareTo(fecha1) == 0 && j<copia.length-1)
			{
				cantidad++;

				if(copia[j].getAccidentIndicator().equals("Yes"))
				{
					conInfraccion++;
				}
				else
				{
					sinInfraccion++;
				}

				pagadoInfraccion += copia[j].getPenalty1();

				j++;
				aux = copia[j].getTicketIssueDate().split("T");
				fecha1 =  convertirFecha(aux[0]);
			}

			conInfraccion = (conInfraccion*100)/cantidad;
			sinInfraccion = (sinInfraccion*100)/cantidad;
			conInfraccion++;

			retornar.put(fecha.toString(), new VOcantidadFecha(fecha.toString(), cantidad, sinInfraccion, conInfraccion, pagadoInfraccion));
		}

		Iterable<String> rango = retornar.keys(fechaInicial.toString(), fechaFinal.toString());
		Iterator<String> iterador = rango.iterator();
		while(iterador.hasNext())
		{
			VOcantidadFecha cFecha = retornar.get(iterador.next());
			System.out.println("");
			System.out.println("Fecha: " + cFecha.darFecha() );
			System.out.println("Cantidad infracciones: " + cFecha.darCantidad());
			System.out.println("Porcentaje infracciones sin accidente : "  + cFecha.darSin() + "%");
			System.out.println("Porcentaje infracciones con accidente : "  + cFecha.darCon() + "%");
			System.out.println("Cantidad por pagar: " + "$" + cFecha.darPorPagar());
			System.out.println("");
		}
	}

	//Parte B
	//1B
	private void  darRankingInfraccionesPorTipo (int nCantidad)
	{
		LinearProbingHashST<String, Cola<VOMovingViolations>> tablitaBonita = new LinearProbingHashST<>();
		String [] tipos=new String[movingViolationsPQueue.size()];
		int j=0;
		for(VOMovingViolations v: movingViolationsPQueue)
		{
			if(tablitaBonita.get(v.getViolationCode())==null)
			{
				Cola<VOMovingViolations> aux= new Cola<>();
				aux.enqueue(v);
				tablitaBonita.put(v.getViolationCode(), aux);
				tipos[j]=v.getViolationCode();
				j++;
			}
			else
			{
				Cola<VOMovingViolations> aux=tablitaBonita.get(v.getViolationCode());
				aux.enqueue(v);
				tablitaBonita.put(v.getViolationCode(), aux);
			}
		}

		VOcantidadTipo [] respuesta=new VOcantidadTipo[tablitaBonita.size()];
		for(int i=0;i<tablitaBonita.size();i++)
		{
			Cola<VOMovingViolations> copia=tablitaBonita.get(tipos[i]);
			int conAcc=0;
			int sinAcc=0;
			int total=0;
			for(VOMovingViolations x: copia)
			{
				if(x.getAccidentIndicator().equals("YES"))
				{
					conAcc++;
					total+=x.getFineAmt();
				}
				else
				{
					sinAcc++;
					total+=x.getFineAmt();
				}

			}
			conAcc = (conAcc*100)/copia.size();
			sinAcc = (sinAcc*100)/copia.size();

			VOcantidadTipo nuevo=new VOcantidadTipo(tipos[i], copia.size(), sinAcc, conAcc, total);
			respuesta[i]=nuevo;
		}

		Sort.ordenarMergeSort(respuesta);

		for(int k=0;k<nCantidad;k++)
		{
			System.out.println("");
			System.out.println("ViolationCode: " + respuesta[k].darTipo());
			System.out.println("Cantidad infracciones: " + respuesta[k].darCantidad());
			System.out.println("Porcentaje infracciones con accidente : " + respuesta[k].darCon()+ "%");
			System.out.println("Porcentaje infracciones sin accidente : " + respuesta[k].darSin()+ "%");
			System.out.println("Cantidad por pagar: " + "$" + respuesta[k].darPorPagar());
			System.out.println("");
		}

	}

	//2B

	public VOCantidadXYCoord ordenanimentoLocalizacionEnArbolBalanceado(double xCoord, double yCoord)
	{
		VOOrdenarxycoord[] copia = new VOOrdenarxycoord[movingViolationsPQueue.size()];

		BST<Double, VOCantidadXYCoord> arbolitoOrdenadito=new BST<>();
		int i = 0;

		for(VOMovingViolations v : movingViolationsPQueue )
		{
			copia[i] = new VOOrdenarxycoord(v.getPenalty1(), v.getX(), v.getY(), v.getAccidentIndicator(), v.getLocation(), v.getAddressId(), v.getStreetSegId());
			i++;
		}

		Sort.ordenarMergeSort(copia);

		int cantidad = 0;
		int sinInfraccion = 0;
		int conInfraccion = 0;
		int pagadoInfraccion = 0;

		for(int j = 0; j<copia.length; j++)
		{
			double xCo = copia[j].darX();
			double yCo = copia[j].darY();
			String location = copia[j].darLocation();
			int adId = copia[j].darAddressId();
			String stId = copia[j].darStreetSegId();

			while(xCo == copia[j].darX() && yCo == copia[j].darY() && j<copia.length)
			{
				cantidad++;

				if(copia[j].accidentIndicator().equals("Yes"))
				{
					conInfraccion++;
				}
				else
				{
					sinInfraccion++;
				}

				pagadoInfraccion += copia[j].darpenalty();

				j++;
			}

			conInfraccion = (conInfraccion*100)/cantidad;
			sinInfraccion = (sinInfraccion*100)/cantidad;

			double llave = xCo + yCo + (xCo/yCo);

			arbolitoOrdenadito.put(llave, new VOCantidadXYCoord(xCo, yCo, cantidad, sinInfraccion, conInfraccion, pagadoInfraccion, location, adId, stId, 0, 0));

			conInfraccion = 0;
			sinInfraccion = 0;
			cantidad = 0;
		}
		System.out.println(arbolitoOrdenadito.size());

		double llaveretornar = xCoord + yCoord + (xCoord/yCoord);

		VOCantidadXYCoord retornar = arbolitoOrdenadito.get(llaveretornar);

		System.out.println("");
		System.out.println("Cantidad infracciones: " + retornar.darCantidad());
		System.out.println("Porcentaje infracciones sin accidente : "  + retornar.darSin() + "%");
		System.out.println("Porcentaje infracciones con accidente : "  + retornar.darCon() + "%");
		System.out.println("Cantidad por pagar: " + "$" + retornar.darPorPagar());
		System.out.println("Location: " + retornar.darLocation());
		System.out.println("AddressId: " + retornar.darAddressId());
		System.out.println("StreetSegId: " + retornar.darStreetSegId());
		System.out.println("");

		return retornar;
	}

	//3B

	public void consultarFranjasEnRango(int valorInicial, int valorFinal)
	{
		LinearProbingHashST<Integer, VOcostoFranjaHoraria[]> tablitaBonita = new LinearProbingHashST<>();
		VOMovingViolations[] copia = new VOMovingViolations[movingViolationsPQueue.size()];
		Integer [] dias=new Integer[movingViolationsPQueue.size()];
		int j=0;
		for(VOMovingViolations v: movingViolationsPQueue)
		{
			copia[j] = v;
			j++;
		}
		
		Sort.ordenarMergeSort(copia);
		
		j=0;
		for(int i=1;i<copia.length-10;i++)
		{
			int k=0;
			VOcostoFranjaHoraria [] horas=new VOcostoFranjaHoraria[24];
			int day=convertirFecha_Hora_LDT(copia[i-1].getTicketIssueDate()).getDayOfYear();
			int day1=convertirFecha_Hora_LDT(copia[i].getTicketIssueDate()).getDayOfYear();
			while(i<copia.length-10&&day==day1)
			{
				
				int cantidad=0;
				String fechaCasual="";
				int hora=0;
				int conAcc=0;
				int sinAcc=0;
				int valorAcumulado=0;
				while(i<copia.length-10&&convertirFecha_Hora_LDT(copia[i-1].getTicketIssueDate()).getHour()==convertirFecha_Hora_LDT(copia[i].getTicketIssueDate()).getHour())
				{
					if(copia[i].getAccidentIndicator().equals("Yes"))
					{
						conAcc++;
					}
					else
					{
						sinAcc++;
					}
					cantidad++;
					hora=convertirFecha_Hora_LDT(copia[i].getTicketIssueDate()).getHour();
					fechaCasual=copia[i].getTicketIssueDate();
					valorAcumulado+=copia[i].getFineAmt();
					if(i<copia.length) {
					i++;}
					else
						break;
					System.out.println(i);
				}
				conAcc = (conAcc*100)/cantidad;
				sinAcc = (sinAcc*100)/cantidad;
				
				horas[k]=new VOcostoFranjaHoraria(fechaCasual, hora, cantidad, sinAcc, conAcc, valorAcumulado);
				k++;
				i++;
				day1=convertirFecha_Hora_LDT(copia[i].getTicketIssueDate()).getDayOfYear();
			}
			
			tablitaBonita.put(day, horas);
			dias[j]=convertirFecha_Hora_LDT(copia[i].getTicketIssueDate()).getDayOfYear();
			j++;
			
			
		}
		
		for(j=0;j<tablitaBonita.size();j++)
		{
			
			if(tablitaBonita.contains(dias[j])) {
			VOcostoFranjaHoraria [] clon=tablitaBonita.get(dias[j]);
			for(int i=1;i<clon.length&&clon[i]!=null;i++)
			{
				if(clon[i].darValorAcumulado()>=valorInicial&&clon[i].darValorAcumulado()<=valorFinal)
				{
					System.out.println("");
					System.out.println("Esta fecha esta en el rango: " +clon[i].darFecha());
					System.out.println("Cantidad infracciones: " + clon[i].darCantidad());
					System.out.println("Porcentaje infracciones sin accidente : "  + clon[i].darSin() + "%");
					System.out.println("Porcentaje infracciones con accidente : "  + clon[i].darCon() + "%");
					System.out.println("Valor total acumulado: " + "$" + clon[i].darValorAcumulado());
				}
				
			}}
		}
	


	}


	//Parte C
	//1C
	public void darInformacionLocalizacion (int pLocalizacion)
	{
		int cantidad = 0;
		int sinInfraccion = 0;
		int conInfraccion = 0;
		int pagadoInfraccion = 0;
		String stId = "";

		for(VOMovingViolations v: movingViolationsPQueue)
		{
			if(v.getAddressId() == pLocalizacion)
			{
				cantidad++;

				if(v.getAccidentIndicator().equals("Yes"))
				{
					conInfraccion++;
				}
				else
				{
					sinInfraccion++;
				}

				pagadoInfraccion += v.getPenalty1();

				stId = v.getStreetSegId();

			}

		}

		conInfraccion = (conInfraccion*100)/cantidad;
		sinInfraccion = (sinInfraccion*100)/cantidad;


		System.out.println("");
		System.out.println("Cantidad infracciones: " + cantidad);
		System.out.println("Porcentaje infracciones sin accidente : "  + sinInfraccion + "%");
		System.out.println("Porcentaje infracciones con accidente : "  + conInfraccion+ "%");
		System.out.println("Cantidad por pagar: " + "$" + pagadoInfraccion);
		System.out.println("StreetSegId: " + stId);
		System.out.println("");
	}

	//2C

	public void infraccionesRangoHora(String pHoraInicial, String pHoraFinal)
	{
		int cantidad = 0;
		int sinInfraccion = 0;
		int conInfraccion = 0;
		int pagadoInfraccion = 0;
		LinearProbingHashST<String, Integer> violationCode = new LinearProbingHashST<String, Integer>();
		Cola<String> vioCode = new Cola<String>();
		for(VOMovingViolations v: movingViolationsPQueue)
		{
			String[] aux = v.getTicketIssueDate().split("T");
			String hora = aux[1].substring(0, 8);

			if(hora.compareTo(pHoraInicial)>0 && hora.compareTo(pHoraFinal)<0)
			{
				cantidad++;

				if(v.getAccidentIndicator().equals("Yes"))
				{
					conInfraccion++;
				}
				else
				{
					sinInfraccion++;
				}

				pagadoInfraccion += v.getPenalty1();

				if(violationCode.get(v.getViolationCode()) == null)
				{
					violationCode.put(v.getViolationCode(), 1);
					vioCode.enqueue(v.getViolationCode());
				}
				else 
				{
					int cant = violationCode.get(v.getViolationCode());
					cant++;
					violationCode.put(v.getViolationCode(), cant);
				}

			}

		}

		conInfraccion = (conInfraccion*100)/cantidad;
		sinInfraccion = (sinInfraccion*100)/cantidad;
		conInfraccion++;


		System.out.println("");
		System.out.println("Cantidad infracciones: " + cantidad);
		System.out.println("Porcentaje infracciones sin accidente : "  + sinInfraccion + "%");
		System.out.println("Porcentaje infracciones con accidente : "  + conInfraccion+ "%");
		System.out.println("Cantidad por pagar: " + "$" + pagadoInfraccion);	

		for(String v : vioCode)
		{
			System.out.println(v + ": "  + violationCode.get(v) );
		}

		System.out.println("");
	}

	//3C
	public void rankingCoordenadas(int nCoordenadas)
	{
		int cantidad = 0;
		int sinInfraccion = 0;
		int conInfraccion = 0;
		int pagadoInfraccion = 0;
		int conPorcen = 0;
		int sinPorcen = 0;
		double llave = 0;
		LinearProbingHashST<Double, VOCantidadXYCoord> coor = new LinearProbingHashST<Double, VOCantidadXYCoord>();
		Cola<Double> colaCoor = new Cola<Double>();
		for(VOMovingViolations v: movingViolationsPQueue)
		{

			llave = v.getX() + v.getY() + (v.getX()/v.getY());

			if(coor.get(llave) == null)
			{
				sinInfraccion = 0;
				conInfraccion = 0;
				if(v.getAccidentIndicator().equals("Yes"))
				{
					conInfraccion++;
				}
				else
				{
					sinInfraccion++;
				}

				conPorcen = (conInfraccion*100);
				sinPorcen = (sinInfraccion*100);
				conInfraccion++;

				coor.put(llave, new VOCantidadXYCoord(v.getX(), v.getY(), 1, sinInfraccion, conInfraccion, v.getPenalty1(), v.getLocation(), v.getAddressId(), v.getStreetSegId(), conPorcen, sinPorcen));
				colaCoor.enqueue(llave);
			}
			else 
			{
				VOCantidadXYCoord xyCooor = coor.get(llave);
				cantidad = xyCooor.darCantidad();
				cantidad++;
				conInfraccion = xyCooor.darCon();
				sinInfraccion = xyCooor.darSin();
				pagadoInfraccion = xyCooor.darPorPagar();
				pagadoInfraccion += v.getPenalty1();

				if(v.getAccidentIndicator().equals("Yes"))
				{
					conInfraccion++;
				}
				else
				{
					sinInfraccion++;
				}

				conPorcen = (conInfraccion*100)/cantidad;
				sinPorcen = (sinInfraccion*100)/cantidad;

				coor.put(llave, new VOCantidadXYCoord(v.getX(), v.getY(), cantidad, sinInfraccion, conInfraccion, pagadoInfraccion, v.getLocation(), v.getAddressId(), v.getStreetSegId(), conPorcen, sinPorcen));

			}


		}

		int i = 0;
		VOCantidadXYCoord[] imprimir = new VOCantidadXYCoord[colaCoor.size()];
		for(Double v : colaCoor)
		{
			imprimir[i] = coor.get(v);
			i++;
		}
		Sort.ordenarMergeSort(imprimir);

		for(int j = 0; j<nCoordenadas; j++)
		{
			System.out.println("");
			System.out.println("Cantidad infracciones: " + imprimir[j].darCantidad());
			System.out.println("Porcentaje infracciones sin accidente : "  + imprimir[j].darSinPor() + "%");
			System.out.println("Porcentaje infracciones con accidente : "  + imprimir[j].darConPor() + "%");
			System.out.println("Cantidad por pagar: " + "$" + imprimir[j].darPorPagar());
			System.out.println("Location: " + imprimir[j].darLocation());
			System.out.println("AddressId: " + imprimir[j].darAddressId());
			System.out.println("StreetSegId: " + imprimir[j].darStreetSegId());
			System.out.println("");
		}

		System.out.println(colaCoor.size());

	}

	//4C
	public void darGraficaPorCodigo()
	{
		int cantidad = 0;
		double porcentaje = 0;
		int total = movingViolationsPQueue.size();
		String vC = "";
		LinearProbingHashST<String, Integer> vioCode = new LinearProbingHashST<String, Integer>();
		Cola<String> colaVioCode = new Cola<String>();
		for(VOMovingViolations v: movingViolationsPQueue)
		{
			vC = v.getViolationCode();
			if(vioCode.get(vC) == null)
			{
				vioCode.put(vC, 1);
				colaVioCode.enqueue(vC);
			}
			else
			{
				cantidad = vioCode.get(vC);
				cantidad++;
				vioCode.put(vC, cantidad);
			}
		}
		
		int i = 0;
		VOCantViolationCode[] imprimir = new VOCantViolationCode[colaVioCode.size()];
		
		for(String v : colaVioCode)
		{
			imprimir[i] = new VOCantViolationCode(v, vioCode.get(v));
			i++;
		}
		
		Sort.ordenarMergeSort(imprimir);
		
		System.out.println("Reporte Agregado de Infracciones por Código 2018:");
		for(VOCantViolationCode v: imprimir)
		{
			System.out.println(v.getViolationCode() + " | " + (v.getCantidad()*100)/total + "%");
		}
		System.out.println("");
	}
	/**
	 * Convertir fecha a un objeto LocalDate
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @return objeto LD con fecha
	 */
	private static LocalDate convertirFecha(String fecha)
	{
		return LocalDate.parse(fecha, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	}
	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaaTHH:mm:ss con dd para dia, mm para mes y aaaa para agno, HH para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fechaHora)
	{
		return LocalDateTime.parse(fechaHora, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'.000Z'"));
	}
}