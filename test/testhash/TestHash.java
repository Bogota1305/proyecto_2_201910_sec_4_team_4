package testhash;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import controller.Controller;
import model.data_structures.Cola;
import model.data_structures.LinearProbingHashST;
import model.data_structures.SeparateChainingHashST;
import model.vo.VOMovingViolations;

public class TestHash {
	
	private Cola<VOMovingViolations> x;
	
	private Controller manager;
	
	private LinearProbingHashST<Integer, Integer> linearHash;
	
	private SeparateChainingHashST<Integer, Integer> separateHash;
	
	
	
	@Before
	public void setup1()
	{
		x=new Cola<VOMovingViolations>();
		linearHash=new LinearProbingHashST<>(2005);
		separateHash=new SeparateChainingHashST<>(2006);
		for(int i=0;i<10000;i++)
		{
			Integer a=(int) (Math.random()*1000);
			linearHash.put(i, a);
			separateHash.put(i, a);
			
		}
		System.out.println(linearHash.size());
	}
	@Test
	public void test1()
	{
		setup1();
		
	}
	@Test
	public void testgetLM()
	{
		setup1();
		for(int i=0;i<linearHash.size();i++)
		{
			linearHash.get(i);
		}
	}
	@Test
	public void testgetSH()
	{
		setup1();
		for(int i=0;i<separateHash.size();i++)
		{
			separateHash.get(i);
		}
	}

}
